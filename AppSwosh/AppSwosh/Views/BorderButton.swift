//
//  BorderButton.swift
//  AppSwosh
//
//  Created by Priitsh Sawant on 29/03/18.
//  Copyright © 2018 Priitsh Sawant. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
    }

}
