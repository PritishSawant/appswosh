//
//  Player.swift
//  AppSwosh
//
//  Created by Priitsh Sawant on 30/03/18.
//  Copyright © 2018 Priitsh Sawant. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague:String!
    var selectedSkillLevel:String!
}
